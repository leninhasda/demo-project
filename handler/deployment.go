package handler

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"

	"github.com/mytaxi/app/handler/render"
	"github.com/mytaxi/app/service"
)

// Deployment is a handler for deployed services
type Deployment struct {
	DepSvc service.Deployments
}

// ListServices list all running services
func (d *Deployment) ListServices(w http.ResponseWriter, r *http.Request) {
	ds, err := d.DepSvc.GetDeployments("default")
	if err != nil {
		log.Println(err)
		render.JSON(w, http.StatusInternalServerError, render.M{"error": err.Error()})
		return
	}

	render.JSON(w, http.StatusOK, ds)
}

// ListServicesByGroup lists filtered services by group
func (d *Deployment) ListServicesByGroup(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	ds, err := d.DepSvc.GetDeploymentsByGroupName("default", vars["appGroup"])
	if err != nil {
		log.Println(err)
		render.JSON(w, http.StatusInternalServerError, render.M{"error": err.Error()})
		return
	}

	render.JSON(w, http.StatusOK, ds)
}
