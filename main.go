package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"time"

	"github.com/gorilla/mux"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"

	"github.com/mytaxi/app/handler"
	"github.com/mytaxi/app/infrastructure/k8s"
	"github.com/mytaxi/app/service"
)

var (
	wait time.Duration
	port int
)

func init() {
	flag.DurationVar(&wait, "gracefulwait", 15, "graceful timeout period, default: 15 (seconds)")
	flag.IntVar(&port, "port", 8080, "port number app should listen, default: 8080")
}

func main() {
	flag.Parse()

	// initialize
	client := getK8sClient()
	dRepo := k8s.Deployments(client)
	dSvc := service.Deployment(dRepo)
	dh := handler.Deployment{dSvc}

	r := mux.NewRouter()
	r.HandleFunc("/services", dh.ListServices)
	r.HandleFunc("/services/{appGroup}", dh.ListServicesByGroup)

	srv := &http.Server{
		Addr:         fmt.Sprintf("0.0.0.0:%d", port),
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      r,
	}

	go func() {
		log.Printf("Server listening on port: http://0.0.0.0:%d\n", port)
		if err := srv.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Kill, os.Interrupt)
	<-stop

	// gracefull wait
	ctx, cancel := context.WithTimeout(context.Background(), wait*time.Second)
	defer cancel()

	// shutdown
	srv.Shutdown(ctx)
	log.Println("shutting down")
	os.Exit(0)
}

func getK8sClient() *kubernetes.Clientset {
	// resolve path
	home := os.Getenv("HOME")
	if home == "" {
		home = os.Getenv("USERPROFILE")
	}

	kubeConfigPath := filepath.Join(home, ".kube", "config")

	// create client
	config, err := clientcmd.BuildConfigFromFlags("", kubeConfigPath)
	if err != nil {
		log.Panic("error building config", err.Error())
	}

	client, err := kubernetes.NewForConfig(config)
	if err != nil {
		log.Panic("error creating new k8s client from config", err.Error())
	}

	return client
}
