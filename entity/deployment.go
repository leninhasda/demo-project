package entity

// Deployment represents a k8s deployment
// only the fields we need for now
type Deployment struct {
	Name             string `json:"name"`
	ApplicationGroup string `json:"applicationGroup"`
	RunningPodsCount int    `json:"runningPodsCount"`
}
