services:
	kubectl apply -f ./services.yaml

build:
	go build -o ./app .

test:
	go test -v ./...