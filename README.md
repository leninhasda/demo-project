# Demo Project

I used go modules for dependency management so the project has to be outside `GOPATH`. 

**Few things:**

* I tried to follow the best practice i usually do
* Used BDD style testing using `ginkgo` but only used in infrastructure layer as others have simple implementation and i wanted to keep that way
* Note that test will require a running minikube cluster. I skipped mocking for this but in actual project i will definitely have that.
* Kept logging to minimal and only when error occurs. 

## install and run

1. Start the minikube local Kubernetes server
2. Copy this project to outside `GOPATH`. `cd` into it and run `go get` to download dependencies
3. Run `make services` or `kubectl apply -f ./services.yaml` to apply the services
4. Run `go run main.go` to start the server
5. (optional) Run test `make test` or `go test -v ./...` 

## Tasks

* [`kubectl`](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
* A Kubernetes Cluster, created e.g. via [minikube](https://kubernetes.io/docs/setup/minikube/) or [Docker for Mac](https://docs.docker.com/docker-for-mac/kubernetes/).

Please apply the services to your local Kubernetes cluster by executing `kubectl apply -f ./services.yaml`.

### 1. Expose information on all pods in the cluster

Add an endpoint to the service that exposes all pods running in the cluster in namespace `default`:

```
GET `/services`
[
  {
    "name": "first",
    "applicationGroup": "alpha",
    "runningPodsCount": 2
  },
  {
    "name": "second",
    "applicationGroup": "beta",
    "runningPodsCount": 1
  },
  ...
]
```

### 2. Expose information on a group of applications in the cluster

Create an endpoint in your service that exposes the pods in the cluster in namespace `default` that are part of the same `applicationGroup`:

```
GET `/services/{applicationGroup}`
[
  {
    "name": "foobar",
    "applicationGroup": "<applicationGroup>",
    "runningPodsCount": 1
  },
  ...
]
```