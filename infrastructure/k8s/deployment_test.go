package k8s_test

import (
	"os"
	"path/filepath"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"

	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"

	. "github.com/mytaxi/app/infrastructure/k8s"
)

func getKubConfigPath() string {
	h := os.Getenv("HOME")
	return filepath.Join(h, ".kube", "config")
}

var _ = Describe("Deployment", func() {

	var client *kubernetes.Clientset

	BeforeEach(func() {
		config, err := clientcmd.BuildConfigFromFlags("", getKubConfigPath())
		if err != nil {
			panic(err)
		}

		client, err = kubernetes.NewForConfig(config)
		if err != nil {
			panic(err)
		}
	})

	Describe("GetAll", func() {
		It("should return all deployments by namespace", func() {

			d := Deployments(client)

			ds, err := d.GetAll("default")

			Expect(err).To(BeNil())
			Expect(len(ds)).NotTo(Equal(0))
		})
	})

	Describe("GetAllByGroupName", func() {
		It("should return all deployments by namespace and group name", func() {

			d := Deployments(client)

			ds, err := d.GetAllByGroupName("default", "alpha")

			Expect(err).To(BeNil())
			Expect(len(ds)).To(Equal(1))
			Expect(ds[0].ApplicationGroup).To(Equal("alpha"))

			ds, err = d.GetAllByGroupName("default", "beta")

			Expect(err).To(BeNil())
			Expect(len(ds)).To(Equal(2))
			Expect(ds[0].ApplicationGroup).To(Equal("beta"))
		})
	})
})
