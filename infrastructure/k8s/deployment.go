package k8s

import (
	"errors"

	k8sErr "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"

	"github.com/mytaxi/app/entity"
)

var (
	// ErrNotFound represents not found error
	ErrNotFound = errors.New("error: not found")
)

// DeploymentRepo is an interface for accessing kubernetes deployments
type DeploymentRepo interface {
	GetAll(ns string) ([]entity.Deployment, error)
	GetAllByGroupName(ns, grp string) ([]entity.Deployment, error)
}

// Deployments accepts a kubernetes client and return a DeploymentRepo
func Deployments(k8sClient kubernetes.Interface) DeploymentRepo {
	return &deployments{k8sClient}
}

// deployments structures implements DeploymentRepo
type deployments struct {
	k8sClient kubernetes.Interface
}

func (d *deployments) get(ns string, opts metav1.ListOptions) ([]entity.Deployment, error) {
	deps, err := d.k8sClient.AppsV1().Deployments(ns).List(opts)

	if k8sErr.IsNotFound(err) {
		return nil, ErrNotFound
	} else if statusErr, ok := err.(*k8sErr.StatusError); ok {
		return nil, errors.New("error: getting deployments - " + statusErr.Error())
	} else if err != nil {
		return nil, err
	}

	eDeps := make([]entity.Deployment, len(deps.Items))
	for i, d := range deps.Items {
		eDeps[i] = entity.Deployment{
			Name:             d.GetName(),
			ApplicationGroup: d.Labels["applicationGroup"],
			RunningPodsCount: int(*d.Spec.Replicas),
		}
	}

	return eDeps, err
}

// GetAll returns all deployments by namespace
func (d *deployments) GetAll(ns string) ([]entity.Deployment, error) {
	opts := metav1.ListOptions{}

	return d.get(ns, opts)
}

// GetAllByGroupName returns all deployments by namespace and application group name
func (d *deployments) GetAllByGroupName(ns, grp string) ([]entity.Deployment, error) {
	opts := metav1.ListOptions{
		LabelSelector: "applicationGroup=" + grp,
	}

	return d.get(ns, opts)
}
