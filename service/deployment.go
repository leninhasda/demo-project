package service

import (
	"errors"
	"log"

	"github.com/mytaxi/app/entity"
	"github.com/mytaxi/app/infrastructure/k8s"
)

type Deployments interface {
	GetDeployments(ns string) ([]entity.Deployment, error)
	GetDeploymentsByGroupName(ns, grp string) ([]entity.Deployment, error)
}

func Deployment(dr k8s.DeploymentRepo) Deployments {
	return &deployments{dr}
}

type deployments struct {
	depRepo k8s.DeploymentRepo
}

func (d *deployments) GetDeployments(ns string) ([]entity.Deployment, error) {
	ds, err := d.depRepo.GetAll(ns)

	if err != nil {
		log.Println("error:", err.Error())
		if err == k8s.ErrNotFound {
			return nil, nil
		}
		return nil, errors.New("something went wrong")
	}

	return ds, nil
}

func (d *deployments) GetDeploymentsByGroupName(ns, grp string) ([]entity.Deployment, error) {
	ds, err := d.depRepo.GetAllByGroupName(ns, grp)

	if err != nil {
		log.Println("error:", err.Error())
		if err == k8s.ErrNotFound {
			return nil, nil
		}
		return nil, errors.New("something went wrong")
	}

	return ds, nil
}
